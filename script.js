let Disciplina1 = [];
class Disciplina {
  constructor(nome){
    this.nome= nome;
  }
}
function cadastrarDisciplina(){
  let nome = byId("nomeDaDisciplina").value;
  let novaDisciplina = new Disciplina(nome);
  Disciplina1.push(novaDisciplina);
  exibirDisciplina();
}
function exibirDisciplina() {
  let tabela = byId("tabela");
  let lista = "<ol>"
  for(let novaDisciplina of Disciplina1) {
    lista += `<li>${novaDisciplina.nome} </li>`
  }
   lista += "</ol>"
  tabela.innerHTML = lista;
}
function byId(id) {
  return document.getElementById(id);
}
  
 